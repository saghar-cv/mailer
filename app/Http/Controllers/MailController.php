<?php


namespace App\Http\Controllers;


use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class MailController extends Controller
{

    public function index(Request $request)
    {

        $to_name = 'Geev Server';
        $to_email = 'info.mehranfar@gmail.com';
        $data = [
            'name' => $request->name,
            'phone' => $request->phone,
            'email' => $request->email,
            'subject' => $request->subject,
            'message' => $request->message
        ];
        Mail::send('emails.mail', ['data' => $data], function ($message) use ($to_name, $to_email) {
            $message->to($to_email, $to_name)
                ->subject('فرم تماس با ما');
            $message->from('saghar.mojdehi@gamil.com', 'Saghar');
        });

    }
}
